#!/usr/bin/env node

const cheerio = require("cheerio")
const request = require("request")
const fs      = require("fs")
const links   = require("./link.json")
const readlineSync = require("readline-sync")
const open = require("open")

console.log('\033c')
rispostaUtente()

function rispostaUtente(){
	let risp = readlineSync.question('------------------\nCOMANDI\n------------------\n1 ----> scarica il db o aggiorna i link\n2 ----> naviga nei link\n\ncosa scegli?   ');
	console.log('\033c')
	if(risp === '1'){
		if(!fs.existsSync("./serie")) fs.mkdirSync("./serie")
		for(var link of links){
			listaSerieTV(link)
		}
	}else if (risp === '2') {
		navigaLink('serie');
	}else{
		rispostaUtente()
	}
}

function navigaLink(path){
	let cat = fs.readdirSync("./"+path);
	console.log("__________________________\n"+stampaLista(cat))
	let num = readlineSync.question("__________________________\nNUMERO: ")
	console.log('\033c')
	if(!cat[num]) navigaLink(path)
	if(path.split('/')[1]){
		leggiJson(path + cat[num], "stagioni")
	}else{
		navigaLink('serie/' + cat[num] + "/")
	}
}

function leggiJson(path, prop1, prop2){
	let serie = require('./' + path)
	let string = ""
	let tipo = ""

	if(prop2 !== undefined){
		tipo = "EPISODI"
		string = stampaJson(serie[prop1][prop2], tipo)
	}else{
		tipo = "STAGIONI"
		string = stampaJson(serie[prop1],tipo)
	}
	console.log("__________________________\n" + string);
	let stagione = readlineSync.question(`_______________\n${tipo}-->   `)
	console.log('\033c')
	if(!serie[prop1][stagione]){
		leggiJson(path, prop1)
	}else if(prop2){
		open(serie[prop1][prop2][stagione].link.replace('embed-','').replace('.html',''));
	}else{
		leggiJson(path, prop1, stagione)
	}
}

function stampaJson(obj,string){
	let ret = ""
	if(string==="EPISODI"){
		for(var num of Object.keys(obj)){
			ret = ret + "---> "+string+" " + obj[num].episodio + "\n    " + obj[num].link + "\n\n"
		}
	}else {
		for(var num of Object.keys(obj)){
			ret = ret + "---> "+string+" " + num + "\n"
		}
	}
	return ret
}

function stampaLista(arr){
	let risp = ""

	for(var num in arr){
		if(!arr[num].includes(".") || arr[num].includes(".json")) risp = risp + num + "  -  " + arr[num].replace(".json",'') + "\n"
	}
	return risp
}

function listaSerieTV(url){
	let $         = {},
	linkSerie = [],
	folder = url.split('/')

	if(folder[folder.length-1]===''){
		folder = folder[folder.length-2]
	}else{
		folder= folder[folder.length-1]
	}
	if(!fs.existsSync("./serie/" + folder)) fs.mkdirSync("./serie/" + folder)

	request.get(url,function(err,body){
		$ = cheerio.load(body.body)
		if(err) console.log(err)
		$(".box-link-serie").each(function(i, element){
			linkSerie.push(element.attribs.href)
		})

		for(var link of linkSerie){
			recuperaUrl(link, folder)
		}
	})
}


function recuperaUrl(url, folder){
	request.get(url,function(err,body){
		if(err) console.log(err)
		let json = arrayBody(body.body)
		fs.writeFileSync("./serie/" + folder + "/" + json.serie.replace(new RegExp('/','g'),'-') +".json",JSON.stringify(json,null,4))
	})
}

function arrayBody(body){
	let $        = cheerio.load(body),
	serie    = {},
	strSerie = "",
	array    = []
	$("span.player-overlay").each(function(i, element){
		if(!strSerie) strSerie=element.attribs["meta-serie"]
		if(element.attribs["meta-embed"]){
			array.push({
				stagione : element.attribs["meta-stag"],
				episodio : element.attribs["meta-ep"],
				link     : element.attribs["meta-embed"]
			})
		}
	})
	return {
		serie    : strSerie,
		stagioni : stagioni(array)
	}
}


function stagioni(array){
	let obj={};
	for(const row of array){
		if(!obj[row.stagione]) obj[row.stagione] = []
		obj[row.stagione].push({
			episodio : row.episodio,
			link     : row.link
		})
	}
	return obj
}